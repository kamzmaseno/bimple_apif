ActiveAdmin.register Agent do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
#permit_params  :name
 


# end



controller do
    def permitted_params
      params.permit agent: [:name, :location,:dateOfBirth, :image, :url]
    end
  end

  form do |f|
    f.inputs "Agent Details" do
    f.input :name
    f.input :location
    f.input :dateOfBirth
    f.input :image, :required => false, :as => :file
# Will preview the image when the object is edited
    end
    f.actions
  end

  show do |ad|
      attributes_table do
        row :name
        row :location
        row :image do
          image_tag(ad.image.url(:thumb))
        end
        # Will display the image on show object page
      end
    end
end
