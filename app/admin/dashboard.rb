ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do

      column do
        panel "Recent Sales" do
         table_for Sale.order('id desc').limit(10) do
            column("Item")   {|order| (order.item)                                    }
            column("units")   {|order| (order.units)                                  }
            column("unit price")   {|order| (order.units_price)                       }
            column("confirmation")   {|order| status_tag(order.Confirmation)                                    }
            column("Agent"){|order| link_to(order.agent.name, '#')                  }
            column("Total")   {|order| number_to_currency order.total_price                       }
          end
        end
        end
        
         column do
        panel "Recently added customers" do
         table_for Customer.order('id desc').limit(4) do
            column("Name")   {|order| (order.name)                                    }
            column("additional details")   {|order| (order.additional_details)                                  }
          end
        end
        end
      end
    end
end
