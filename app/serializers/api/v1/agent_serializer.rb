class Api::V1::AgentSerializer < Api::V1::BaseSerializer
  attributes :name

  has_many :sales
  has_many :customers
  has_many :expenses

  ActiveModel::Serializer.setup do |config|
     config.embed = :ids
  end

end