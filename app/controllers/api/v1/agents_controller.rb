class Api::V1::AgentsController < Api::V1::BaseController
  include ActiveHashRelation
  #before_filter :generate_aunthentication_token 

  def show
    agent = Agent.find(params[:id])
    render(json: Api::V1::AgentSerializer.new(agent).to_json)
  end

  def index
  	agents = Agent.all

  	agents = apply_filters(agents, params)

  	render(
  		json: ActiveModel::ArraySerializer.new(
  			agents,
  			each_serializer: Api::V1::AgentSerializer,
  			root: 'agents',
  			)
  		)
  end

  def create
    @agent = Agent.new(agent_params)

    if @agent.save
      render json: @agent, status: :created, location: @agent
    else
      render json: @agent.errors, status: :unprocessable_entity
    end
  end
  def update
    @agent = Agent.find(params[:id])

    if @agent.update(agent_params)
      head :no_content
    else
      render json: @agent.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @agent.destroy

    head :no_content
  end

  private

    def set_agent
      @agent = Agent.find(params[:id])
    end

    def agent_params
      params[:agent]
    end

  
end