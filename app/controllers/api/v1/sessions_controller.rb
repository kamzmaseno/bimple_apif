=begin
class Api::V1::SessionsController < Api::V1::BaseController
  def create
    agent = Agent.find_by(EmployId: create_params[:EmployId])
    if agent && agent.authenticate(create_params[:EmployId])
      self.current_agent = agent
      render(
        json: Api::V1::SessionSerializer.new(agent, root: false).to_json,
        status: 201
      )
    else
      return api_error(status: 401)
    end
  end

  private
  def create_params
    params.require(:agent).permit(:EmployId)
  end
end
=end
