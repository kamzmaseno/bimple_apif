class AgentsController < ApplicationController
  before_action :set_agent, only: [:show, :update, :destroy]

  def index
    @agents = Agent.all

    render json: @agents
  end

 
  def show
    render json: @agent
  end

  def create
    @agent = Agent.new(agent_params)

    if @agent.save
      render json: @agent, status: :created, location: @agent
    else
      render json: @agent.errors, status: :unprocessable_entity
    end
  end

 
  def update
    @agent = Agent.find(params[:id])

    if @agent.update(agent_params)
      head :no_content
    else
      render json: @agent.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @agent.destroy

    head :no_content
  end

  private

    def set_agent
      @agent = Agent.find(params[:id])
    end

    #no white listing here.no param is permitted

    def agent_params
      params[:agent]
    end
end