class Customer < ActiveRecord::Base
	belongs_to :agent
	#validates :name, :address, :contacts, :additional_details, presence:true
	accepts_nested_attributes_for :agent,
                                   reject_if: 
                                   proc { |attributes| attributes['type'].blank?},
                                   allow_destroy: true
end
