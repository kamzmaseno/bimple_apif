class Expense < ActiveRecord::Base
	belongs_to :agent
	#validates :total_amount, :amount_used, :amount_balance, presence:true
	accepts_nested_attributes_for :agent,
                                   reject_if: 
                                   proc { |attributes| attributes['type'].blank?},
                                   allow_destroy: true
end
