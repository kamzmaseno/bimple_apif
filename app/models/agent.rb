class Agent < ActiveRecord::Base
    has_many :sales
	has_many :expenses
	has_many :customers
	
	has_attached_file :image, :styles => { :medium => "300x300#", :thumb => "200x200#" }
	validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg",     "image/png"] } 
	
	#makes sure that the agent name passed isnt empty
	validates :name, presence: true
	#validates :EmployId, presence: true #numericality: { only_integer: true }
	accepts_nested_attributes_for :sales,:customers,:expenses,
                                   reject_if: 
                                   proc { |attributes| attributes['type'].blank?},
                                   allow_destroy: true
end