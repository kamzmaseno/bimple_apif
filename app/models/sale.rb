class Sale < ActiveRecord::Base
	belongs_to :agent
	#validates :item, :units, :units_price, :total_price,:Confirmation, presence:true
	accepts_nested_attributes_for :agent,
                                   reject_if: 
                                   proc { |attributes| attributes['type'].blank?},
                                   allow_destroy: true
end
