Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :sales
  resources :expenses
  resources :customers
  resources :agents

  namespace :api do
    namespace :v1 do
      resources :agents, only: [:index, :create, :show, :update, :destroy]
 end
end
end
