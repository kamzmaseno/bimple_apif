class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
    	t.string :name
    	t.string :address
    	t.string :contacts
    	t.text :additional_details

      t.timestamps null: false
    end
  end
end
