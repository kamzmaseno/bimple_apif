class AddPaperclipFieldsToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :image_file_name,    :string
    add_column :agents, :image_content_type, :string
  end
end
