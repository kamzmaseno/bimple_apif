class AddAgentForeignKeys < ActiveRecord::Migration
  def change
  	add_column :sales, :agent_id, :integer
  	add_column :customers, :agent_id, :integer
  	add_column :expenses, :agent_id, :integer
  end
end
