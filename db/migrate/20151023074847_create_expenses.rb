class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
    	t.decimal :total_amount
    	t.decimal :amount_used
    	t.decimal :amount_balance

      t.timestamps null: false
    end
  end
end
