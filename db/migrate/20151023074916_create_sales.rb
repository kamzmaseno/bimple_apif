class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
    	t.string :item
    	t.integer :units
    	t.decimal :units_price
    	t.decimal :total_price
    	t.text :descriptions
    	t.boolean :Confirmation

      t.timestamps null: false
    end
  end
end
